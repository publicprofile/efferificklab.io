---
layout: post
title:  "Register Spilling"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

register spilling - (By analogy with spilling the contents of an overfull container) When a compiler is generating machine code and there are more live variables than the machine has registers and it has to transfer or "spill" some variables from registers to memory.

https://www.webster-dictionary.org/definition/register%20spilling
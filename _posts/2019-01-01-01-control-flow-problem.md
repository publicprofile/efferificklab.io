---
layout: post
title:  "Control Flow Problem"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

\[...] The control-flow problem asks, "Which procedures may the expression <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="f"><mi>f</mi></math> be in the call <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="left-parenthesis f x right-parenthesis"><mo stretchy="false">(</mo><mi>f</mi><mi>x</mi><mo stretchy="false">)</mo></math>?".

In programming languages, and static analysis in particular, the higher-order control-flow problem refers to the fact that the precise target of a function call may not be obvious.

The problem afflicts functional languages in the form of first-class functions, and it afflicts object-oriented languages in the form of dynamically dispatched methods. 

\[0] http://matt.might.net/articles/implementation-of-kcfa-and-0cfa/
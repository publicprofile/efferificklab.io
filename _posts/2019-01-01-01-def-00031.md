---
layout: post
title:  "Capacity Miss"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

Capacity misses occur when the requested data has been expelled because too much different data has entered the cache since the last reference: the “reuse distance” is too large. \[0]

\[0] Beyls, Kristof, and Erik D’Hollander. "Reuse distance as a metric for cache behavior." Proceedings of the IASTED Conference on Parallel and Distributed Computing and systems. Vol. 14. 2001.
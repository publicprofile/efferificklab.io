---
layout: post
title:  "Meet Over All Paths Solution"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

The desired solution to a set of simultaneous equations.

Given a global data flow analysis problem \[gdfap], _the_ _meet_ _over_ _all_ _paths_ solution for a program can be interpreted informally as the calculation for each statement in the program of the maximum information, relevant to the gdfap, which is true along every possible execution path from the starting point of the program to that particular statement.

There is proof of nonexistence algorithm to compute the meet over all paths for monotone frameworks. There is proof that the MOP exists for monotone frameworks which meet the _distributivity_ condition. Instead of computing the MOP for monotone frameworks, the maximal fixed point is computed.

It appears generally true that what one searches for in a data flow problem is what we shall call the _meet_ _over_ _all_ _paths_ (MOP) solution. That is let <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="PATH left-parenthesis n right-parenthesis"><mtext>PATH</mtext><mo stretchy="false">(</mo><mi>n</mi><mo stretchy="false">)</mo></math> denote the set of paths from the initial node to the node <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="n"><mi>n</mi></math> in some flow graph. Then we really want <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="logical-and Underscript upper P element-of PATH left-parenthesis n right-parenthesis Endscripts f Subscript p Baseline left-parenthesis 0 right-parenthesis"><munder><mo>⋀<!-- ⋀ --></mo><mrow class="MJX-TeXAtom-ORD"><mi>P</mi><mo>∈<!-- ∈ --></mo><mtext>PATH</mtext><mo stretchy="false">(</mo><mi>n</mi><mo stretchy="false">)</mo></mrow></munder><msub><mi>f</mi><mi>p</mi></msub><mo stretchy="false">(</mo><mn>0</mn><mo stretchy="false">)</mo></math> for each <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="n"><mi>n</mi></math>. It is this function, the MOP solution, that in any practical data flow problem we can think of, express the desired information.

[0] Monotone Data Flow Analysis Frameworks. John B, Kam and Jeffrey D. Ullman.


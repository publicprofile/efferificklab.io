---
layout: post
title:  "Austria"
date:   2020-03-15 00:00:00 +0000
categories: [list]
---

# Emergency phone numbers

* Ambulance: 144
* Police: 133
* Firefighters: 122
* Health hotline (in case of suspected infection only!): 1450
* Coronavirus information hotline: 0800 555 621 

# Reliable sources of information in English

* [The Metropole](https://metropole.at/coronavirus-in-austria)
* [Wien.info](https://www.wien.info/en/travel-info/coronavirus-information)
* [Social Minizterium](https://www.sozialministerium.at/en/Coronavirus/FAQ-Coronavirus.html)
* [Wien.gv.at](https://www.wien.gv.at/english/)
* [Wikipedia: 2020 Coronavirus Pandemic in Austria](https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Austria)

# [Basic recommendations](https://www.epa.gov/pesticide-registration/list-n-disinfectants-use-against-sars-cov-2)

The Centers for Disease Control and Prevention (CDC) has provided many resources to assist healthcare facilities, community sites, businesses, and households in preventing the spread of the virus. One CDC site provides the following recommendations:

* Routinely clean all frequently touched surfaces in the workplace, such as workstations, countertops, and doorknobs, using a detergent or soap and water before disinfection.
     
* Wear disposable gloves when cleaning and disinfecting surfaces. Gloves should be discarded after each cleaning. If reusable gloves are used, those gloves should be dedicated for cleaning and disinfection of surfaces for COVID-19 and should not be used for other purposes. Consult the manufacturer’s instructions for cleaning and disinfection products used. Clean hands immediately after gloves are removed.
     
* For disinfection, diluted household bleach solutions, alcohol solutions with at least 70 percent alcohol, and most common EPA-registered household disinfectants should be effective.
     
* Diluted household bleach solutions can be used if appropriate for the surface. Follow manufacturer’s instructions for application and proper ventilation. Check to ensure the product is not past its expiration date. Never mix household bleach with ammonia or any other cleanser. Unexpired household bleach will be effective against coronaviruses when properly diluted. Prepare a bleach solution by mixing 5 tablespoons (1/3 cup) bleach per gallon of water or 4 teaspoons bleach per quart of water.


## Garbage

* Metals
* Plastic
* Tetrapack
* [\[source\]](https://translate.google.com/translate?sl=de&tl=en&u=https%3A%2F%2Fwww.wien.gv.at%2Fumwelt%2Fma48%2F)

![Plastic bottles recycling](/img/20200315_131940.jpg)

* White glass and colored glass

![Trash cans for white glass and colored glass](/img/20200315_131915.jpg)

* Biological garbage

![Trash cans for compost](/img/20200315_131948.jpg)

* Clothes and shoes

![Clothes and shoes recycling](/img/20200315_131925.jpg)

* Everything else

![Everything else](/img/20200315_131931.jpg)


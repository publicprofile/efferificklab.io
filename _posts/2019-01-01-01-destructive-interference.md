---
layout: post
title:  "Destructive Interference"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

Different applications displace each other’s data from shared caches. \[0]

\[0] Zhang, Yuanrui, et al. "Optimizing data layouts for parallel computation on multicores." Parallel Architectures and Compilation Techniques (PACT), 2011 International Conference on. IEEE, 2011.
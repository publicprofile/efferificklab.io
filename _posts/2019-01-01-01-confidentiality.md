---
layout: post
title:  "Confidentiality"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

Confidentiality is the property that guarantees private information can only be accessed by authorized parties. [...] It is enforced through a number of mechanisms, including encrption, spatial, temporal, or virtual separation, and mediation through access checks. When ensuring confidentiality, it is not enough for implementations to be functionally correct. In addition to computing the correct result, private data must not leak to unauthorized parties by any means. Such leakage can happen through unforseen information channels, called _side_-_channels_. Any measurable property of a computer implementation has the potential to be a side-channel.

[...] Confidentiality is at risk when code of different parties share resources as a processor core, processor, processor package, memory bus, DRAM, cache, or disk. Side-channels can exist both in time-sharing scenarios, where state persists between context switches or in concurrent scenarios where processes directly contentd for resources.

Mcilroy, Ross, et al. "Spectre is here to stay: An analysis of side-channels and speculative execution." arXiv preprint arXiv:1902.05178 (2019).
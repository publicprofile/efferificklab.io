---
layout: post
title:  "Conflict Miss"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

Conflict misses occur when the requested data has been expelled because too much different data has entered the associated cache lines. [0]

[0] Beyls, Kristof, and Erik D’Hollander. "Reuse distance as a metric for cache behavior." Proceedings of the IASTED Conference on Parallel and Distributed Computing and systems. Vol. 14. 2001.
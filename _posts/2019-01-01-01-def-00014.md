---
layout: post
title:  "Arity"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

The number of arguments of a function or predicate

\[0] Greco, Sergio, and Cristian Molinaro. "Datalog and logic databases." Synthesis Lectures on Data Management 7.2 (2015): 1-169.
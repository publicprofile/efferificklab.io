---
layout: post
title:  "Heap Reachability"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

The exsitence of paths of references between objects.

\[0] Ter-Gabrielyan, Arshavir, Alexander J. Summers, and Peter Müller. "Modular Verification of Heap Reachability Properties in Separation Logic." arXiv preprint arXiv:1908.05799 (2019).
---
layout: post
title:  "Compilation Order"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

Formally, _a_ _compilation_ _order_ refers to an order in which the compilation units in a program (e.g. functions or traces) are being compiled or recompiled.
It should not be confused with the order of the various optimizations phases in a compilation, which is related with internal design rather than usage of a compiler.

[0] Ding, Yufei, et al. "Finding the limit: examining the potential and complexity of compilation scheduling for JIT-based runtime systems." ACM SIGARCH Computer Architecture News 42.1 (2014): 607-622.